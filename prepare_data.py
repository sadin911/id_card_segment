# -*- coding: utf-8 -*-
"""
Created on Thu Oct 11 13:54:11 2018

@author: chonlatid.d
"""

import glob
import xmlparse as xmlp
import xml.etree.ElementTree as ET
from PIL import Image
import os
file_list = glob.glob("Z:\Keng\DATA\IDCARD\image\*xml")
strout = []

for i in range(0,len(file_list)):
    
    tree = ET.parse(file_list[i])
    root = tree.getroot()

    fname = []
    
    fname = root.find('filename').text
    #img_path = r"Z:\Keng\DATA\IDCARD\image" + "\\" + fname + ".jpg"
    img_path = os.path.join("Z:\Keng\DATA\IDCARD\image",fname)
    img = Image.open(img_path)
    p = root.find('object')
    i=0;
    if type(p) != None:
        print("p is not None")
    for obj in root.findall('object'):
        p = obj.find('name').text
        if obj.find('name').text == "old_id" :
            box = obj.find('bndbox')
            xmin = box.find('xmin').text
            xmax = box.find('xmax').text
            ymin = box.find('ymin').text
            ymax = box.find('ymax').text
            area = (int(xmin),int(ymin),int(xmax),int(ymax))
            cropped_img = img.crop(area)
            img_savepath = os.path.join('Z:\\Keng\\DATA\\IDCARD\\fake\\train\\' , 'crop' + fname)
            cropped_img.save(img_savepath)
            i+=1