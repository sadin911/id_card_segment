# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 15:20:36 2018

@author: chonlatid.d
"""

from keras.layers import Input, Dense, Reshape, Flatten, Dropout
from keras.layers import BatchNormalization, Activation, ZeroPadding2D, MaxPooling2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras.models import load_model 
import matplotlib.pyplot as plt

import sys
import numpy as np
from PIL import Image, ImageFont, ImageDraw, ImageEnhance
import glob
from pathlib import Path

import resnet

class DCCLASSIFIER():
    def __init__(self):
        self.img_rows = 128
        self.img_cols = 128
        self.channels = 3
        self.img_shape = (self.img_rows, self.img_cols, self.channels)
        self.model = resnet.ResnetBuilder.build_resnet_18((self.channels, self.img_rows, self.img_cols), 10)
        optimizer = Adam(0.0001, 0.5)
        self.model.compile(loss='mse',
              optimizer=optimizer,
              metrics=['accuracy'])
        self.model.summary()
        #self.model.load_weights('0_wdis.h5')
    def train(self, epochs, batch_size=32, save_interval=1):
        file_list_valid = glob.glob(r"D:\Keng\DATA\data_seg\*.jpg")
        
       
        for epoch in range(0,epochs):
            idx_valid = np.random.randint(0, len(file_list_valid), batch_size)
            valid_train = np.zeros((batch_size,self.img_rows,self.img_cols,self.channels))
            valid_batch = np.zeros((batch_size,self.img_rows,self.img_cols,self.channels))
            for i in range(0,len(idx_valid)):
                valid = np.ones((self.img_rows, self.img_cols,self.channels))
                valid = Image.fromarray(valid.astype('uint8'), 'RGB')
                valid_image = Image.open(file_list_valid[idx_valid[i]])
                rotate_param = np.random.uniform(-1*15 , 15)
                valid_image = valid_image.rotate(rotate_param,resample = Image.BILINEAR,expand = True, fillcolor =(0,0,0))   #rotate image
                valid = valid.rotate(rotate_param,resample = Image.BILINEAR,expand = True, fillcolor =(0,0,0))   #rotate image
                valid = valid.resize((self.img_rows,self.img_cols))
                valid_image = valid_image.resize((self.img_rows,self.img_cols))
                valid_image = np.asarray(valid_image)
                valid_image = valid_image / 127.5 - 1.
                valid = np.asarray(valid)
                valid_train[i][:][:] = valid_image
                valid_batch[i][:][:] = valid
                
            d_loss = self.model.train_on_batch(valid_train, valid_batch)
           
            print ("%d [D loss: %f, acc.: %.2f%%]" % (epoch, d_loss[0], 100*d_loss[1]))
            
            if epoch % save_interval == 0:
                self.model.save_weights(str(epoch)+"_wres.h5")
            if epoch % 1 == 0:
                
                idx_valid = np.random.randint(0, len(file_list_valid), 1)
                background = Image.open(file_list_valid[idx_valid[0]])
                rotate_param = np.random.uniform(-1*15 , 15)
                background = background.rotate(rotate_param,resample = Image.BILINEAR,expand = True, fillcolor =(0,0,0))   #rotate image
                background = background.resize((self.img_rows,self.img_cols))
                background = np.asarray(background)
                background = np.expand_dims(background,axis=0)
                background = background / 127.5 - 1.
                
                foreground = self.model.predict(background)
                foreground[:,:,:,1] *=0
                foreground[:,:,:,2] *=0
                foreground = (foreground)*255
                foreground = np.reshape(foreground,(128,128,3))
                foreground = Image.fromarray(foreground.astype('uint8'), 'RGB')
                
                background = np.reshape(background,(128,128,3))
                background = (background+1)*127.5
                background = Image.fromarray(background.astype('uint8'), 'RGB')
                #Image.alpha_composite(background.convert('RGBA'), foreground.convert('RGBA')).save("debug.png")
                bimg = Image.blend(background, foreground, 0.7)
                try:
                    bimg.save('debug.jpg')
                    background.save('bg.jpg')
                    foreground.save('fg.jpg')
                    self.model.save(str(0)+"_mres.h5")
                    self.model.save_weights(str(0)+"_wres.h5")
                except IOError as e:
                    print("I/O error({0}): {1}".format(e.errno, e.strerror))
    
                
            
                
if __name__ == '__main__':
    dcclassifier = DCCLASSIFIER()
    dcclassifier.train(epochs=100000, batch_size=32, save_interval=500)
    #dcgan.test()
    #dcgan.train(epochs=100000, batch_size=128, save_interval=10)